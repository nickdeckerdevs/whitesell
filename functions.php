<?php
/**
* @package   Master
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// check compatibility
if (version_compare(PHP_VERSION, '5.3', '>=')) {

    // bootstrap warp
    require(__DIR__.'/warp.php');
}


function get_string_between( $string, $start, $end = null ){
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if( is_null( $end ) ) {
    	return substr( $string, $ini );
    }
    if ($ini == 0) return '';
    $ini += strlen($start);
	$len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

function delete_between_strings($string, $start, $end = null ) {
	$start_position = strpos( $string, $start );
	if( is_null( $end ) ) {
		$deleted_text = substr( $string, $start_position );
	} else {
		$end_position = strpos( $string, $end );
		if( $start_position === false || $end_position === false ) return $string;
		$deleted_text = substr( $string, $start_position, ( $end_position + strlen( $end ) ) - $start_position );	
	}
	return str_replace( $deleted_text, '', $string );
}

function remove_garbage_characters( $string ) {
	$string = str_replace( ' &nbsp;', '', $string );
	$string = str_replace( '&nbsp; ', '', $string );
	return trim( $string );
}
require_once('kint/Kint.class.php');