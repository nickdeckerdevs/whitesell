<?php
/**
* @package   Master
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// get theme configuration
include($this['path']->path('layouts:theme.config.php'));
$post = get_post(2);  //get the home page
?>
<!DOCTYPE HTML>
<html lang="<?php echo $this['config']->get('language'); ?>" dir="<?php echo $this['config']->get('direction'); ?>"  data-config='<?php echo $this['config']->get('body_config','{}'); ?>'>

<head>
<?php echo $this['template']->render('head'); ?>
</head>

<body class="home home-page <?php echo $this['config']->get('body_classes'); ?>">
	<div class="uk-container uk-container-center">

		<?php if ($this['widgets']->count('toolbar-l + toolbar-r')) : ?>
		<div class="tm-toolbar uk-clearfix uk-hidden-small">

			<?php if ($this['widgets']->count('toolbar-l')) : ?>
			<div class="uk-float-left"><?php echo $this['widgets']->render('toolbar-l'); ?></div>
			<?php endif; ?>

			<?php if ($this['widgets']->count('toolbar-r')) : ?>
			<div class="uk-float-right"><?php echo $this['widgets']->render('toolbar-r'); ?></div>
			<?php endif; ?>

		</div>
		<?php endif; ?>

		<?php if ($this['widgets']->count('logo + headerbar')) : ?>
		<div class="black-background">
			<div class="tm-headerbar uk-clearfix uk-hidden-small page-center">
				<?php if ($this['widgets']->count('logo')) : ?>
				<a class="tm-logo" href="<?php echo $this['config']->get('site_url'); ?>"><?php echo $this['widgets']->render('logo'); ?></a>
				<?php endif; ?>

				<?php if ($this['widgets']->count('menu + search')) : ?>
					<nav class="tm-navbar uk-navbar">

						<?php if ($this['widgets']->count('menu')) : ?>
						<?php echo $this['widgets']->render('menu'); ?>
						<?php endif; ?>

						<?php if ($this['widgets']->count('offcanvas')) : ?>
						<a href="#offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>
						<?php endif; ?>

						<?php if ($this['widgets']->count('search')) : ?>
						<div class="uk-navbar-flip">
							<div class="uk-navbar-content uk-hidden-small"><?php echo $this['widgets']->render('search'); ?></div>
						</div>
						<?php endif; ?>

						<?php if ($this['widgets']->count('logo-small')) : ?>
						<div class="uk-navbar-content uk-navbar-center uk-visible-small"><a class="tm-logo-small" href="<?php echo $this['config']->get('site_url'); ?>"><?php echo $this['widgets']->render('logo-small'); ?></a></div>
						<?php endif; ?>

					</nav>
					<?php endif; ?>
			</div>
		</div>
		
		<?php endif; ?>
		
        <div class="chained-select page-center">
            I am an <span class="dropdown first-option" data-option="buyer">Buyer / Purchasor
                <ul class="dropdown-list" style="display: none;">
                    <li data-option="buyer">Buyer / Purchasor</li>
                    <li data-option="engineer">Engineer / Designer</li>
                    <li data-option="qm">Quality Manager</li>
                    <li data-option="acquisitions">Acquisitions</li>
                </ul>
            </span>
                
                and<br>I’m looking for <span class="dropdown second-option">Material / Piece Price Savings
                <ul class="dropdown-list import-list" style="display: none;">
                </ul>
            </span>.

            <div class="cta-container">
                <a href="industries.html" class="cta cta-yellow">Learn How</a>
            </div>

        </div>
		<div class="honeycomb-hideout">

			<div class="home-content-container page-center">
				<div class="home-content-header">
					<?php if(get_post_custom_values('home-content-header')) {
						echo '<h2>'.get_post_meta($post->ID, 'home-content-header', true).'</h2>';
					} ?>
				</div>
				<div class="grey-background">
					<?php if(get_post_custom_values('home-grey-area')) {
						echo get_post_meta($post->ID, 'home-grey-area', true);
					} ?>
				</div>
				<div class="white-background shapes-background bottom-angle">
					<?php if(get_post_custom_values('home-white-area') && get_post_custom_values('home-white-area-header')) {
						echo '<h2>'.get_post_meta($post->ID, 'home-white-area-header', true).'</h2>';
						echo '<p>'.get_post_meta($post->ID, 'home-white-area', true);
						if(get_post_custom_values('home-yellow-cta')) { ?>
							<div class="cta-container">
								<a class="cta cta-yellow" href="<?php echo get_post_meta($post->ID, 'home-yellow-cta-link', true); ?>"><?php echo get_post_meta($post->ID, 'home-yellow-cta', true); ?></a>
							</div>
						<?php }
					} ?>
				</div>

				<?php 
				$background = get_post_custom_values('home-image-url') ? get_post_meta($post->ID, 'home-image-url', true) : '';
				?>
				<div class="testimonial-image" style="background: url(<?php echo $background; ?>) center center no-repeat;">
					<div class="orange-overlay">
						<?php if(get_post_custom_values('home-image-quote') && get_post_custom_values('home-image-name-and-title')) { ?>
							<h3 class="quote"><?php echo get_post_meta($post->ID, 'home-image-quote', true); ?></h3>
							<h4><?php echo get_post_meta($post->ID, 'home-image-name-and-title', true); ?></h4>
						<?php } ?>
					</div>
				</div>
					
				<div class="industries-container white-background">
					<?php if(get_post_custom_values('home-second-white-area-header')) { ?>
						<h2><?php echo get_post_meta($post->ID, 'home-second-white-area-header', true); ?></h2>
					<?php } ?>
					<ul class="industries-links-row">
						<?php if(get_post_custom_values('home-icon-1')) { ?>
						<li>
							<a href="<?php echo get_post_meta($post->ID, 'home-icon-1-link', true); ?>">
								<img src="<?php echo get_post_meta($post->ID, 'home-icon-1', true); ?>" />
							</a>
						</li>
						<?php } ?>
						<?php if(get_post_custom_values('home-icon-2')) { ?>
						<li>
							<a href="<?php echo get_post_meta($post->ID, 'home-icon-2-link', true); ?>">
								<img src="<?php echo get_post_meta($post->ID, 'home-icon-2', true); ?>" />
							</a>
						</li>
						<?php } ?>
						<?php if(get_post_custom_values('home-icon-3')) { ?>
						<li>
							<a href="<?php echo get_post_meta($post->ID, 'home-icon-3-link', true); ?>">
								<img src="<?php echo get_post_meta($post->ID, 'home-icon-3', true); ?>" />
							</a>
						</li>
						<?php } ?>
						<?php if(get_post_custom_values('home-icon-4')) { ?>
						<li>
							<a href="<?php echo get_post_meta($post->ID, 'home-icon-4-link', true); ?>">
								<img src="<?php echo get_post_meta($post->ID, 'home-icon-4', true); ?>" />
							</a>
						</li>
						<?php } ?>
					</ul>
					<ul class="industries-links-row">
						<?php if(get_post_custom_values('home-icon-5')) { ?>
						<li>
							<a href="<?php echo get_post_meta($post->ID, 'home-icon-5-link', true); ?>">
								<img src="<?php echo get_post_meta($post->ID, 'home-icon-5', true); ?>" />
							</a>
						</li>
						<?php } ?>
						<?php if(get_post_custom_values('home-icon-6')) { ?>
						<li>
							<a href="<?php echo get_post_meta($post->ID, 'home-icon-6-link', true); ?>">
								<img src="<?php echo get_post_meta($post->ID, 'home-icon-6', true); ?>" />
							</a>
						</li>
						<?php } ?>
						<?php if(get_post_custom_values('home-icon-7')) { ?>
						<li>
							<a href="<?php echo get_post_meta($post->ID, 'home-icon-7-link', true); ?>">
								<img src="<?php echo get_post_meta($post->ID, 'home-icon-7', true); ?>" />
							</a>
						</li>
						<?php } ?>
					</ul>
					<?php if(get_post_custom_values('home-grey-cta') && get_post_custom_values('home-grey-cta-link')) { ?>
					<div class="center-flex">
						<div class="cta-container">
							<a href="<?php echo get_post_meta($post->ID, 'home-grey-cta-link', true); ?>" class="cta cta-grey">
								<?php echo get_post_meta($post->ID, 'home-grey-cta', true); ?>
							</a>
						</div>
					</div>
					<?php } ?>

				</div>
				<div class="white-background no-padding">
					<div class="yellow-shading">&nbsp;</div>
				</div>

				
			</div>
		</div>

		<?php if ($this['widgets']->count('footer + debug') || $this['config']->get('warp_branding', true) || $this['config']->get('totop_scroller', true)) : ?>
		<footer id="tm-footer" class="tm-footer">
			<div class="page-center">
				<?php
					echo $this['widgets']->render('footer');
				?>
			</div>
		</footer>
		<?php endif; ?>

	</div>

	<?php echo $this->render('footer'); ?>

	<?php if ($this['widgets']->count('offcanvas')) : ?>
	<div id="offcanvas" class="uk-offcanvas">
		<div class="uk-offcanvas-bar"><?php echo $this['widgets']->render('offcanvas'); ?></div>
	</div>
	<?php endif; ?>
<script>
var spreadsheet = '1DFM12Ni9tHuoPCViRUU0DAL1xJJPa-6O_yz_5nzFhaI';
var personas = {};



jQuery(document).ready(function($) {
	function getSpreadsheetData(callback, another) {
		url = 'https://spreadsheets.google.com/feeds/list/'+ spreadsheet+'/od6/public/values?alt=json'
		$.getJSON(url, function(data) {
			var entry = data.feed.entry;
			var count = 1;
			$(entry).each(function() {
				var optionName = this.gsx$persona.$t;
				if(typeof(personas[optionName]) == 'object') {
					personas[optionName][count] = {};
					personas[optionName][count].text = this.gsx$text.$t;
					personas[optionName][count].url = this.gsx$url.$t;
				} else {
					personas[optionName] = {};
					personas[optionName][count] = {};
					personas[optionName][count].text = this.gsx$text.$t;
					personas[optionName][count].url = this.gsx$url.$t;
				}
				count++;
			})
			callback(personas, another);
		});
	}
	function operateDropDown(dropdown) {
		dropdown.find('.dropdown-list').toggle();
	}

	function createOptionsList(personas, callback, target) {
		var list = '';
		selectedOption = target ? target : $('.first-option').data('option');
		var count = 0;
		var firstKey = '';
		for(var key in personas[selectedOption]) {
			if(count == 0) firstKey = key;
			var url = personas[selectedOption][key].url;
			var text = personas[selectedOption][key].text;
			list += '<li data-target="'+url+'">'+text+'</li>';
			count++;
		}
		callback(list, personas[selectedOption][firstKey]);
	}

	function fillImportList(listHtml, firstOption) {
		var importListHtml = firstOption.text+'<ul class="dropdown-list import-list" style="display: none;">'+listHtml+'</ul>';
		$('.second-option').data('option', firstOption.url).html(importListHtml);
		changeTargetUrl($('.second-option').data('option'));
		var listHeight = $('.import-list').outerHeight();
		document.styleSheets[0].addRule('.dropdown .dropdown-list.import-list:after', 'top: '+(listHeight - 2)+'px!important');
		importFirstOption = [];
	}

	function changeTargetUrl(target) {
		$('.chained-select .cta-container a').attr('href', target);
	}

	function blink(target, time, times) {
		if(!times) times = 1;
		if(!time) time = 500;
		console.log(target);
		$(target).on('click', function() {
			// console.log('stopping?')
			// $(this).stop();
			//return;
		});
		for(var j = 0; j < times; j++) {
			$(target).fadeTo(time, 0.1).fadeTo(time, 1.0);    
		}
	}

	

	$('.dropdown').on('click', function() {
		operateDropDown($(this));
	});

	$('.chained-select').on('click', '.first-option li', function() {
		var $list = $(this).parent().parent().html();
		var $htmlArray = $list.split('<');
		var $previousText = $htmlArray[0];
		var $text = $(this).text();
		var $html = $list.replace($previousText, $text);
		var $id = $(this).data('option')
		$(this).parent().parent().data('option', $id).html($html);
		createOptionsList(personas, fillImportList, $(this).data('option'));
		blink('.second-option', 700, 1);
	});

	$('.chained-select').on('click', '.import-list li', function() {
		var $list = $(this).parent().parent().html();
		var $htmlArray = $list.split('<');
		var $previousText = $htmlArray[0];
		var $text = $(this).text();
		var target = $(this).data('target');
		$('.second-option').html($list.replace($previousText, $text)).data('option', target);
		changeTargetUrl($('.second-option').data('option'));
		$('.chained-select a').toggleClass('active-hover').delay(600).queue(function(next) {
			$(this).toggleClass('active-hover');
			next();
		});
	});


	/* sticky sticky */

	var header = $('header');
	var logo = $('logo');
	

	if(!$('body').hasClass('home')) {
		if(!header.hasClass('sticky')) header.addClass('sticky');
	} else {
		getSpreadsheetData(createOptionsList, fillImportList);
	}

	$(window).on('scroll', function() {
		if($('body').hasClass('home')) {
			var scrollPosition = $(this).scrollTop();
			var menuItem = $('header > nav.menu > ul > li:first-of-type > a');
			if(scrollPosition > 40) {  //the sticky header height that we want to transform the header at
				if(!header.hasClass('sticky')) header.addClass('sticky');
				if(!logo.hasClass('sticky')) logo.addClass('sticky');
				var newMenuItem = menuItem.contents()
					.filter(function() {
						return this.nodeType === 3
					}).remove();
			} else {
				if(header.hasClass('sticky')) header.removeClass('sticky');
				if(logo.hasClass('sticky')) logo.removeClass('sticky');
				if(menuItem.html().indexOf('Home') != 0) {
					menuItem.prepend('Home');	
				}
			}	
		} 
		
	})

	$('.parallax').parallax({
		speed : 0.19
	});
});


(function($) {
	$.fn.parallax = function(options) {
        var windowHeight = $(window).height();
        // Establish default settings
        var settings = $.extend({
            speed        : 0.15
        }, options);
        // Iterate over each object in collection
        return this.each( function() {
        // Save a reference to the element
	        var $this = $(this);
	        // Set up Scroll Handler
	        $(document).scroll(function(){
			    var scrollTop = $(window).scrollTop();
	            var offset = $this.offset().top;
	            var height = $this.outerHeight();




					    // Check if above or below viewport
				if (offset + height <= scrollTop || offset >= scrollTop + windowHeight) {
					return;
				}
				var yBgPosition = Math.round((offset - scrollTop) * settings.speed);
		                // Apply the Y Background Position to Set the Parallax Effect
			    $this.css('background-position', 'center ' + yBgPosition + 'px');
	                
	        });
		});
	}
}(jQuery));



</script>

</body>
</html>